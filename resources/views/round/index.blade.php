@extends('layouts.mainpanel')

@section('title', 'All coffee rounds')

@section('panel-body')
@if($num_active == 0)
<a href="{{ url('rounds/create') }}" class="btn btn-lg btn-success">Start new round</a>
<br><br>
@endif

@forelse($rounds as $round)
<p><a href="{{ url('rounds/'.$round->id) }}">Round by <strong>{{ is_null($round->user_id) ? 'system' : $round->user->full_name }}</strong> ({{ $round->active
	? $round->created_at->diffForHumans()
	: 'processed '.$round->updated_at->diffForHumans() }})</a></p>
@empty
<p>No round currently active.</p>
@endforelse
@endsection