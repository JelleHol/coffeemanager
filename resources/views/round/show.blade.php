@extends('layouts.mainpanel')

@section('title', 'Coffee round '. (is_null($round->user_id) ?
	'(automatic)' : 'by ' . $round->user->name))

@section('panel-body')
@if($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif
@if($message = Session::get('warning'))
<div class="alert alert-warning">
	<p>{{ $message }}</p>
</div>
@endif

@unless($round->active)
<div class="alert alert-warning">
	<p>This round has already been processed.</p>
</div>
@endunless

<div class="clearfix">
	@if(!$order_placed && $round->active)
	<a href="{{ url('orders/create') }}" class="btn btn-success pull-left">Order up!</a>

	<form action="{{ url('orders') }}" method="POST" class="pull-left">
		{{ csrf_field() }}
		<input type="hidden" name="round_id" value="{{ $round->id }}">
		<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
		<input type="hidden" name="ignore" value="1">
		&nbsp;&nbsp;
		<button type="submit" class="btn btn-warning">No thanks</button>
		&nbsp;&nbsp;
	</form>
	@endif

	@if(Auth::user() == $round->user)
	<form action="{{ route('rounds.destroy', $round->id) }}" method="POST" class="pull-left">
		{{ method_field("DELETE") }}
		{{ csrf_field() }}
		<div class="form-group">
			<button class="btn btn-danger" type="submit" onclick="confirm('Are you sure?')">Delete round</button>
		</div>
	</form>
	@endif
</div>

<p>Started at: {{ $round->created_at->format('M j, H:i') }} ({{ $round->created_at->diffForHumans() }})</p>
<p>Retriever: {{ $round->retrieve_self ? $round->user->name : '(random selection)' }}</p>

<h4>Orders</h4>
<ul>
	@forelse($round->orders as $o)
	@if($o->ignore)
	<li><i>{{ $o->user->name }} ignored this round</i></li>
	@else
	<li><b>{{ $o->user->name }}</b> ordered <b>{{ $o->item->description }}</b></li>
	@endif
	@empty
	<li><i>(None)</i></li>
	@endforelse
</ul>
@endsection