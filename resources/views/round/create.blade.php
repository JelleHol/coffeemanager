@extends('layouts.mainpanel')

@section('title', 'Start new round')

@section('panel-body')
<form role="form" method="POST" action="{{ url('rounds') }}">
	{{ csrf_field() }}

	<p>Who should go and get everyone's coffee?</p>

	<div class="radio">
		<label>
			<input type="radio" id="retrieve-option1" name="retrieve-type" value="self" checked>
			Me
		</label>
	</div>
	<div class="radio">
		<label>
			<input type="radio" id="retrieve-option2" name="retrieve-type" value="random">
			Let the system decide
		</label>
	</div>

	<button type="submit" class="btn btn-success">Start</button>

</form>
@endsection