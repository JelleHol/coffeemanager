@extends('layouts.mainpanel')

@section('title', 'Add your order')

@section('panel-body')
<form action="{{ url('items') }}" method="POST">
	{{ csrf_field() }}
	<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
		<label for="new-item">New item&nbsp;</label>
		<input type="text" name="description" maxlength="255">&nbsp;
		<button type="submit" class="btn btn-sm btn-default">Add &gt;</button>
		@if ($errors->has('description'))
		<span class="help-block">
			<strong>{{ $errors->first('description') }}</strong>
		</span>
		@endif
	</div>
</form>

<form action="{{ url('orders') }}" method="POST">
	{{ csrf_field() }}
	
	<div class="form-group{{ $errors->has('item_id') ? ' has-error' : '' }}">
		<div class="row">
			<div class="col-sm-6">
				@forelse($choices as $c)
				<div class="radio">
					<label>
						<input type="radio" id="choice{{ $c->id }}" name="item_id" value={{ $c->id }}>
						{{ $c->description }}
					</label>
				</div>
				@if($loop->iteration % 5 == 0)
			</div>
			<div class="col-sm-6">
				@endif
				@empty
				<p><em>(No items found)</em></p>
				@endforelse
			</div>
		</div>
		@if ($errors->has('item_id'))
		<span class="help-block">
			<strong>{{ $errors->first('item_id') }}</strong>
		</span>
		@endif
	</div>

	<input type="hidden" name="round_id" value="{{ $round->id }}">
	<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

	@unless($round->retrieve_self)
	<div class="checkbox">
		<label>
			<input type="checkbox" id="volunteer" name="volunteer" value="1">
			I volunteer to go and get this round
		</label>
	</div>
	@endunless

	<button type="submit" class="btn btn-primary">Save</button>
</form>
@endsection