<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('items')->count() > 0) return;

        $items = [
            'Coffee (black)',
            'Coffee (sugar)',
            'Coffee (milk)',
            'Coffee (milk & sugar)',
            'Cappuccino',
            'Espresso',
            'Latte',
            'Latte Macchiato',
        ];

        $rows = [];
        foreach ($items as $desc) {
            $rows[] = [
                'description' => $desc,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        }

        DB::table('items')->insert($rows);
    }
}
