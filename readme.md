# CoffeeManager

A Laravel web application to handle the serving of coffee!

Made as an assessment for NetSquare.

## Notes

* Some standard drinks are included in a database seeder.
* For emails to actually work, you need a mail server. For testing purposes, I used [mailtrap.io](https://mailtrap.io).
* The automatic round creation uses Laravel task scheduling, which is called by Cron. ([Documentation](https://laravel.com/docs/5.3/scheduling))
* Other automatic things like round processing use Laravel queueing. This is handled by the queue worker. ([Documentation](https://laravel.com/docs/5.3/queues#running-the-queue-worker))