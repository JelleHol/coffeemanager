<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['round_id','user_id','item_id','volunteer','ignore'];

    public function round()
    {
        return $this->belongsTo('App\Round');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function scopeInRound($query, $round_id)
    {
        return $query->where('round_id', $round_id);
    }

    public function scopeByUser($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }
}
