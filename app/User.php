<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Full name attribute combines first and surname.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return trim($this->name . " " . $this->surname);
    }

    /**
     * Get all rounds created by this user
     */
    public function rounds()
    {
        return $this->hasMany('App\Round');
    }
}
