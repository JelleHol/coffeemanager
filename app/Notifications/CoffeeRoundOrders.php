<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CoffeeRoundOrders extends Notification
{
    use Queueable;

    public $round;

    /**
     * Create a new notification instance.
     *
     * @param \App\Round  $round
     * @return void
     */
    public function __construct($round)
    {
        $this->round = $round;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = new MailMessage();

        if ($this->round->retrieve_self)
            $message->line('Your coffee round has been processed. These are the orders:');
        else
            $message->line('You have been selected to retrieve coffee. These are the orders:');

        foreach ($this->round->orders as $order) {
            if ($order->ignore) continue;
            $message->line('- '.$order->item->description.' for '.$order->user->name);
        }

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
