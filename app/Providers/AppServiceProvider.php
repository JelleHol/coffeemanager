<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Notification;

use Carbon\Carbon;

use App\Round;
use App\User;
use App\Notifications\NewCoffeeRound;
use App\Jobs\ProcessCoffeeRound;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Round creation event listener
        Round::created(function ($round) {
            // Notify users
            Notification::send(User::all(), new NewCoffeeRound($round));

            // Schedule processing job
            $job = (new ProcessCoffeeRound($round))
                ->delay(Carbon::now()->addMinutes(config('coffee.process_delay')));
            dispatch($job);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
