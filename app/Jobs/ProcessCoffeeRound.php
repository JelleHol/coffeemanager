<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

use App\Round;
use App\User;

use App\Notifications\CoffeeRoundComplete;
use App\Notifications\CoffeeRoundOrders;

class ProcessCoffeeRound implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $round;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Round $round)
    {
        $this->round = $round;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $round = $this->round;
        
        // Only process rounds once
        if (!$round->active) return;

        // Get participants / volunteers
        $participants = []; $volunteers = [];
        foreach ($round->orders as $order) {
            $user = $order->user;
            $participants[$user->id] = $user;

            if ($order->volunteer)
                $volunteers[$user->id] = $user;
        }

        // Don't do anything if nobody participated
        if (count($participants) == 0) {
            $round->active = 0;
            $round->save();
            return;
        }

        // Determine retriever
        if ($round->retrieve_self) {
            // Round creator is the retriever
            $retriever = $round->user;
        }
        else {
            // Randomly select retriever
            if (count($volunteers) > 0) {
                $retriever = array_rand($volunteers);
            }
            else {
                $retriever = array_rand($participants);
            }
            $retriever = User::find($retriever);
        }

        // Notify retriever
        $retriever->notify(new CoffeeRoundOrders($round));

        // Notify participants
        unset($participants[$retriever->id]);
        Notification::send($participants, new CoffeeRoundComplete($retriever));

        // Finish up
        $round->active = 0;
        $round->save();
    }
}
