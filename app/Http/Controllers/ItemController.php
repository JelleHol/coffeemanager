<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use App\Item;

class ItemController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->only('description');
        $validator = Validator::make($input, [
            'description' => 'required|string|max:255'
        ]);

        if ($validator->passes()) {
            $item = Item::create($input);
            return redirect()->to('orders/create');
        }

        return back()->with('errors', $validator->errors());
    }
}
