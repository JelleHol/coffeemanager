<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

use Carbon\Carbon;

use App\Http\Requests;
use App\Round;
use App\Order;
use App\User;
use App\Notifications\NewCoffeeRound;
use App\Jobs\ProcessCoffeeRound;

class RoundController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('round.index', [
            'rounds' => Round::orderBy('created_at', 'desc')->get(),
            'num_active' => Round::active()->count(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('round.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('retrieve-type')) {
            $type = $request->input('retrieve-type');
            $user = Auth::user();

            // Create model
            $round = $user->rounds()->create([
                'active' => true,
                'retrieve_self' => $type == 'self'
            ]);

            return redirect()->to('rounds');
        }

        return redirect()->back()->withErrors([
            'retrieve-type' => 'Invalid retrieving choice.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $round = Round::findOrFail($id);

        $order_placed = Order::inRound($id)->byUser(Auth::user()->id)->count() > 0;

        return view('round.show', [
            'round' => $round,
            'order_placed' => $order_placed,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $round = Round::findOrFail($id);
        $round->delete();
        return redirect()->to('rounds');
    }
}
