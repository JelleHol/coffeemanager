<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\Notifications\UserVerificationRequest;
use App\Notifications\UserVerificationComplete;
use App\Jobs\ExpireRegistration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'surname' => 'max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $input = $request->all();
        $validator = $this->validator($input);

        if ($validator->passes()) {
            // Create user
            $user = $this->create($input);
            $token = str_random(30);

            // Add verification entry
            DB::table('user_verifications')->insert([
                'user_id' => $user->id,
                'token' => $token,
                'created_at' => Carbon::now(),
            ]);

            // Send verification link
            $user->notify(new UserVerificationRequest($token));

            // Setup expiration job to delete this registration after x minutes
            $delay = config('registration.expires');
            $expireJob = (new ExpireRegistration($user))
                    ->delay(Carbon::now()->addMinutes($delay));
            dispatch($expireJob);

            return redirect()->to('login')
                ->with('success', 'Verification code sent. Please check your inbox.');
        }

        return back()->with('errors', $validator->errors())
            ->withInput($request->except('password'));;
    }

    /**
     * Verify a user using their token and complete registration.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param string  $token
     * @return \Illuminate\Http\Response
     */
    public function verifyUser(Request $request, $token)
    {
        // Retrieve verification data
        $verification = DB::table('user_verifications')->where('token', $token)->first();

        if (!is_null($verification)) {
            // Check expiration
            $now = Carbon::now();
            $expires = (new Carbon($verification->created_at))->addMinutes(config('registration.expires'));
            $diff = $now->diffInMinutes($expires, false);
            $diffDisp = $now->addMinutes($diff)->diffForHumans();

            if ($diff <= 0) {
                // Expired! Remove this
                DB::table('user_verifications')->where('id', $verification->id)->delete();
                return redirect()->to('login')
                    ->with('error', "Your registration has expired! ($diffDisp)");
            }

            // Find corresponding user
            $user = User::find($verification->user_id);

            if ($user->verified) {
                return redirect()->to('login')
                    ->with('success', "This account has already been verified.");                
            }

            // Verify user
            $user->verified = true;
            $user->save();
            DB::table('user_verifications')->where('id', $verification->id)->delete();

            // Send mail notification
            $user->notify(new UserVerificationComplete());

            return redirect()->to('login')
                ->with('success', "Account successfully verified!");
        }

        return redirect()->to('login')
                ->with('error', "Invalid verification code.");
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
