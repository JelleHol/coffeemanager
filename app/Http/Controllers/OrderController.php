<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Round;
use App\Order;
use App\Item;
use App\User;

use App\Jobs\ProcessCoffeeRound;

class OrderController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $round = Round::active()->latest()->first();

        if (is_null($round)) {
            return redirect()->to('rounds/create');
        }

        $num = Order::inRound($round->id)->byUser(Auth::user()->id)->count();

        if ($num > 0) {
            return redirect()->to('/rounds/'.$round->id)
                ->withWarning('You already placed an order for this round.');
        }

        return view('order.create', [
            'round' => $round,
            'choices' => Item::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'round_id' => 'required|integer',
            'user_id' => 'required|integer',
            'item_id' => 'required_without:ignore|integer',
            'volunteer' => 'boolean',
            'ignore' => 'boolean',
        ], [
            'item_id.*' => 'You need to select an item.'
        ]);

        if ($validator->passes()) {
            $order = Order::create($input);

            if (Order::inRound($input['round_id'])->count() == User::all()->count()) {
                dispatch(new ProcessCoffeeRound($order->round));
                return redirect()->to(url('rounds', $input['round_id']))
                    ->withSuccess('The round will now be processed.');
            }

            return redirect()->to(url('rounds', $input['round_id']));
        }
        return back()->with('errors', $validator->errors());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
