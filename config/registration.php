<?php

return [

	// Number of minutes for a registration to expire
	'expires' => 30,

];