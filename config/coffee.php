<?php

return [

	// Minutes to wait before processing coffee rounds
	'process_delay' => 10,

	// Automatic round will be started after X minutes of last round processing
	'auto_create_delay' => 30,

];